# Peliohjelmointikurssi

## Johdanto

Kurssilla tutustutaan peliohjelmointiin Rust-ohjelmointikielellä käyttäen [Bevy-pelimoottoria](https://bevyengine.org/). Bevy on avoimen lähdekoodin datalähtöinen pelimoottori, jolla monimutkaistenkin pelien tekeminen onnistuu vaivatta Rustin ergonomisuuden takia.

### Opetustavoitteet

Kurssin tavoitteena on ymmärtää peliohjelmoinnin perusteet sekä Rust-ohjelmoinnin perusteet. Kurssilla syvennytään Bevyllä pelien kehittämiseen eikä itse Rustin ohjelmointiin. Kurssilla on tavoitteena ymmärtää Bevyn komponenttien, entiteettien ja systeemien tarkoitus ja toiminta.

## Sisältö

1. [Breakout peli](https://bevyengine.org/examples/games/breakout/) demo
5. Miten tehdään peli palasista, pelikehityksen filosofiaa
1. Mikä on Bevy (ja rust)
1. Asennus (ja optimointi),
2. Entityt ja komponentit käytännössä, Transform ja vektorit, Sprite
3. Mikä on systeemi? Nopeussysteemi ja näppäimistösyöte
4. Miten komponentteja voi hakea ja muuttaa, Query, With ja Without
1. Törmäykset, komennot, Eventit, äänitehosteet

## PILKOTAAN BREAKOUT PELI
1. muokkailaan vakioita
- selitetään syntaxia
- const
- ::
- f32 ja piste perässä
```rust
const BACKGROUND_COLOR: Color = Color::rgb(0.1, 0.1, 0.1);
const BRICK_SIZE: Vec2 = Vec2::new(100., 30.);
```
2. haetaan setup ja luodaan enemmän rivejä palikoita
- puhutaan mainista
- puhutan setupista
- commands

```rust
commands.spawn(WallBundle::new(WallLocation::Left));
commands.spawn(WallBundle::new(WallLocation::Right));
// commands.spawn(WallBundle::new(WallLocation::Bottom));
commands.spawn(WallBundle::new(WallLocation::Top));

```
3. tutustutaan systeemeihin
- rustin ref &
- print_ball()
- Queryt
- _-patterni
- Transformit
- macrot lyhyesti
- komponetit lyhyesti

4. haetaan velocity-systeemi ja muokataan pallon nopeutta
- &mut
- rustin säännöt mut kanssa
- tyhmä virhe esimerkki
```rust
fn apply_velocity(mut query: Query<(&mut Transform, &Velocity)>) {
    for (mut transform, velocity) in &mut query {
        transform.translation.x += velocity.x * /*TIME_STEP*/;
        transform.translation.y += velocity.y * /*TIME_STEP*/;
    }
}
```
5. haetaan input-systeemi ja laitetaan lauta liikkumaan useampaan eri suuntaan (x ja y)
6. kursorilla voidaan lisäillä palloja
7. jotain collisioneita


Jokainen oppitunti koostuu pääosin hands-on tekemisestä. Rustista opetetaan tarvittavat asiat kun niitä tarvitaan.

# todos
- bundle-refactor
- remove setup calculations
- define used libraries
- debug system
- show shitty errors
- refactor move_paddle to support multiple directions